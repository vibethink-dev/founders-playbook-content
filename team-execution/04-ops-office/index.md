---
title: Ops - Office
play: 4
---

## Coworking Spaces

  01. [42 Floors](01-42-floors.webloc)
  02. [Creative Integration](02-creative-integration.webloc)
  03. [Founder Habitat](03-founder-habitat.webloc)
  04. [Galvanize](04-galvanize.webloc)
  05. [Hanahaus](05-hanahaus.webloc)
  06. [1776](06-1776.webloc)
  07. [Herocity](07-herocity.webloc)
  08. [Sandbox Suites](08-sandbox-suites.webloc)
  09. [The Pad](09-the-pad.webloc)
  10. [WeWork](10-wework.webloc)

## Mail Scanning

  01. [Earth Class Mail](01-earth-class-mail.webloc)
  02. [Traveling Mailbox](02-traveling-mailbox.webloc)

