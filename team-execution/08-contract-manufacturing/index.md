---
title: Contract Manufacturing
play: 8
---

## Supply Chain *

  01. [CR Supply Chain Presentation](01-cr-supply-chain-presentation.txt)
  02. [MOU Template](02-mou-template.pdf)

## Contract Manufacturing Resources *

  01. [Dragon Innovation](01-dragon-innovation.webloc)
  02. [Ycombinator resource list - GO FIND](02-ycombinator-resource-list-go-find.txt)
  03. [AQS](03-aqs.webloc)
  04. [Flex](04-flex.webloc)
  05. [Jabil](05-jabil.webloc)
  06. [SF Made](06-sf-made.webloc)

