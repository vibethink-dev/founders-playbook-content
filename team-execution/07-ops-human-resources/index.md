---
title: Ops - Human Resources
play: 7
---

## Best Practices

  01. [Tallwood - HR Best Practices](01-tallwood-hr-best-practices.pdf)
  02. [Robert Siegel - HR Issues In A Startup](02-robert-siegel-hr-issues-in-a-startup.pdf)

## Resources

  01. [RoundPegg](01-roundpegg.webloc)
  02. [Teamphoria](02-teamphoria.webloc)
  03. [Zenefits](03-zenefits.webloc)
  04. [Gusto](04-gusto.webloc)
  05. [Simply Insured](05-simply-insured.webloc)
  06. [Trinet](06-trinet.webloc)
  07. [Lever](07-lever.webloc)
  08. [Greenhouse](08-greenhouse.webloc)
  09. [Recruiterbox](09-recruiterbox.webloc)
  10. [Workable](10-workable.webloc)
  11. [Newton Software](11-newton-software.webloc)

