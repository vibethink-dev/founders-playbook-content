---
title: Ops - Legal
play: 5
---

## Best Practices

  01. [Fenwick and West Legal Resource Guide](01-fenwick-and-west-legal-resource-guide.pdf)
  02. [DLA Piper - Top nine startup legal issues](02-dla-piper-top-nine-startup-legal-issues.pptx)
  03. [Tallwood - Legal Best Practices](03-tallwood-legal-best-practices.pdf)

## Confidentiality Templates

  01. [Confidentiality Agreement One Way Use Entity.DOCX](01-confidentiality-agreement-one-way-use-entity.docx)
  02. [Confidentiality Agreement One Way Use Individuals.DOCX](02-confidentiality-agreement-one-way-use-individuals.docx)
  03. [Confidentiality Agreement Two Way Use Entities.DOCX](03-confidentiality-agreement-two-way-use-entities.docx)
  04. [Confidentiality Agreement Two Way Use Individuals.DOCX](04-confidentiality-agreement-two-way-use-individuals.docx)

## Consulting Templates

  01. [Consulting Agreement Individuals.DOCX](01-consulting-agreement-individuals.docx)
  02. [Consulting Agreement Entity.DOCX](02-consulting-agreement-entity.docx)

## Employee Templates

  01. [Employee At Will Offer Letter.DOCX](01-employee-at-will-offer-letter.docx)
  02. [Employee Invention Non Disclosure Agreement.DOCX](02-employee-invention-non-disclosure-agreement.docx)
  03. [Employee Non Solicitation Non Competition Agreement.DOCX](03-employee-non-solicitation-non-competition-agreement.docx)

## Advisor Templates

  01. [Founder Advisor Standard Template](01-founder-advisor-standard-template.doc)

## Standard Legal Templates

 Acquisition Agreement](acquisition-agreement.docx)
 Administrative and Technology Services Outsourcing](administrative-and-technology-services-outsourcing.docx)
 Affidavit](affidavit.docx)
 Agreement Between Carrier and Shipper](agreement-between-carrier-and-shipper.docx)
 Agreement Between Owner and Contractor](agreement-between-owner-and-contractor.docx)
 Agreement of Purchase and Sale of Shares](agreement-of-purchase-and-sale-of-shares.docx)
 Agreement to Lease](agreement-to-lease.docx)
 Articles of Incorporation](articles-of-incorporation.docx)
 Asset Purchase Agreement](asset-purchase-agreement.docx)
 Bill of Sale](bill-of-sale.docx)
 Bill of Sale_Immovable Property](bill-of-sale-immovable-property.docx)
 Client and Developer Agreement](client-and-developer-agreement.docx)
 Commission Sales Agreement](commission-sales-agreement.docx)
 Confidentiality Agreement](confidentiality-agreement.docx)
 Consignment Agreement](consignment-agreement.docx)
 Consultant Non-Disclosure Agreement](consultant-non-disclosure-agreement.docx)
 Consulting Agreement_Long](consulting-agreement-long.docx)
 Consulting Agreement_Short](consulting-agreement-short.docx)
 Content Provider Agreement](content-provider-agreement.docx)
 Contract for the Manufacture and Sale of Goods](contract-for-the-manufacture-and-sale-of-goods.docx)
 Contract for the Sale of Goods](contract-for-the-sale-of-goods.docx)
 Convertible Debenture](convertible-debenture.docx)
 Convertible Note Agreement](convertible-note-agreement.docx)
 Creation of Servitude](creation-of-servitude.docx)
 Custom Software Development Agreement](custom-software-development-agreement.docx)
 Development Agreement_General](development-agreement-general.docx)
 Development Agreements_Multimedia Publisher](development-agreements-multimedia-publisher.docx)
 Distribution Agreement](distribution-agreement.docx)
 Due Diligence Requisition List](due-diligence-requisition-list.docx)
 Employee Non-Compete Agreement](employee-non-compete-agreement.docx)
 Employee Non-Disclosure Agreement](employee-non-disclosure-agreement.docx)
 Employment Agreement For Technical Employee](employment-agreement-for-technical-employee.docx)
 Employment Agreement_At Will Employee](employment-agreement-at-will-employee.docx)
 Employment Agreement_Key Employee](employment-agreement-key-employee.docx)
 End-User Software License Agreement](end-user-software-license-agreement.docx)
 Equipment Lease Agreement](equipment-lease-agreement.docx)
 Equipment Purchase Agreement](equipment-purchase-agreement.docx)
 Equipment Sales Agreement](equipment-sales-agreement.docx)
 Financing Agreement_Short](financing-agreement-short.docx)
 General By-Laws](general-by-laws.docx)
 Independent Contractor Agreement](independent-contractor-agreement.docx)
 Installment Payment Agreement](installment-payment-agreement.docx)
 Joint Venture Agreement](joint-venture-agreement.docx)
 LLC Operating Agreement](llc-operating-agreement.docx)
 Lease Agreement](lease-agreement.docx)
 Letter of Intent_Acquisition of Business](letter-of-intent-acquisition-of-business.docx)
 License to Use Agreement](license-to-use-agreement.docx)
 Limited Partnership Agreement](limited-partnership-agreement.docx)
 Limited Power of Attorney](limited-power-of-attorney.docx)
 Loan Agreement](loan-agreement.docx)
 Mutual Confidentiality Agreement](mutual-confidentiality-agreement.docx)
 Mutual Indemnification and Hold Harmless Agreement](mutual-indemnification-and-hold-harmless-agreement.docx)
 Mutual Non-Disclosure Agreement](mutual-non-disclosure-agreement.docx)
 Mutual Release](mutual-release.docx)
 Non-Disclosure and Non-Compete Agreement](non-disclosure-and-non-compete-agreement.docx)
 Offer to Purchase Real Estate Property](offer-to-purchase-real-estate-property.docx)
 Option to Buy Agreement_Long](option-to-buy-agreement-long.docx)
 Option to Lease Agreement](option-to-lease-agreement.docx)
 Partnership Agreement](partnership-agreement.docx)
 Payment Guaranty](payment-guaranty.docx)
 Permission to Use Copyrighted Material](permission-to-use-copyrighted-material.docx)
 Personal Guarantee](personal-guarantee.docx)
 Pre-Incorporation Agreement](pre-incorporation-agreement.docx)
 Product Supply Agreement](product-supply-agreement.docx)
 Project Management Agreement](project-management-agreement.docx)
 Promissory Note](promissory-note.docx)
 Reseller Agreement](reseller-agreement.docx)
 Retainer for Attorney](retainer-for-attorney.docx)
 Sales Representative Agreement](sales-representative-agreement.docx)
 Senior Advisor Agreement](senior-advisor-agreement.docx)
 Service Level Agreement](service-level-agreement.docx)
 Shareholders Agreement](shareholders-agreement.docx)
 Software Development and Consulting Services Agreement](software-development-and-consulting-services-agreement.docx)
 Software Distribution Agreement](software-distribution-agreement.docx)
 Software Maintenance Agreement](software-maintenance-agreement.docx)
 Sponsorship Agreement](sponsorship-agreement.docx)
 Stock Purchase Agreement](stock-purchase-agreement.docx)
 Subcontract Agreement](subcontract-agreement.docx)
 Term Sheet for Series A Round of Financing](term-sheet-for-series-a-round-of-financing.docx)
 Warehousing Agreement](warehousing-agreement.docx)
 Web Site Development and Service Agreement](web-site-development-and-service-agreement.docx)
 Website Design Consultation Agreement](website-design-consultation-agreement.docx)

