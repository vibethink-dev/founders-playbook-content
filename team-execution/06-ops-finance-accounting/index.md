---
title: Ops - Finance + Accounting
play: 6
---

## P&L

  01. [MIT Energy Ventures - Financial Template - Corp.](01-mit-energy-ventures-financial-template-corp..xls)

## Project Finance

  01. [MIT Energy Ventures - Proj Fin Model](01-mit-energy-ventures-proj-fin-model.xlsx)

## Operating Plan

  01. [Operating plan template](01-operating-plan-template.xlsx)

## General Accounting

  01. [Tallwood - Finance Best Practices](01-tallwood-finance-best-practices.pdf)

## Example Service Providers

  01. [Lingh Duong](01-lingh-duong.webloc)
  02. [Angela Wong](02-angela-wong.webloc)
  03. [David Faulk](03-david-faulk.webloc)
  04. [Arnold Zippel](04-arnold-zippel.webloc)
  05. [Elaine Banks](05-elaine-banks.webloc)
  06. [Bill Tyndall](06-bill-tyndall.webloc)
  07. [Moss Adams](07-moss-adams.webloc)
  08. [Jeff Faust](08-jeff-faust.webloc)

## Accounting Software

  01. [QuickBooks](01-quickbooks.webloc)
  02. [Xero](02-xero.webloc)
  03. [Wave Apps](03-wave-apps.webloc)
  04. [FreshBooks](04-freshbooks.webloc)
  05. [Bill dot com](05-bill-dot-com.webloc)
  06. [InDinero](06-indinero.webloc)
  07. [Bench](07-bench.webloc)

## Cap Table + Legal Mgmt

  01. [eShares](01-eshares.webloc)
  02. [Capshare](02-capshare.webloc)
  03. [Shoobx](03-shoobx.webloc)

