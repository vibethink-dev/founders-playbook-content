---
title: Board of Directors Meetings
play: 2
---

## Board Meeting Decks

  01. [NextView Board Slide Deck Template](01-nextview-board-slide-deck-template.pptx)
  02. [eG Board Meeting Agenda Template 2010](02-eg-board-meeting-agenda-template-2010.doc)

## Startup Boards

  01. [Samer Hamadeh - What you need to know about startup boards](01-samer-hamadeh-what-you-need-to-know-about-startup-boards.webloc)
  02. [Sequoia - Board Deck Structure](02-sequoia-board-deck-structure.webloc)

