---
Title: How To Tell Your Story
Play: 1
---

## Telling a Story

  1. [Garage Ventures - Perfecting Your Pitch](Garage-Ventures-Perfecting-Your-Pitch.pdf)
      15 MIN READ || Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      
      Credit: [Garage Venture Technologies](http://garage.com)

  2. [David Merkoski - Telling Your Story](David-Merkoski-Telling-Your-Story.pdf)
  3. [Nancy Duarte - Greatest Communicators](https://www.youtube.com/watch?v=1nYFpuc2Umk)
    15 MIN VIDEO
  4. [Andy Raskin - Improving Your Pitch](https://medium.com/firm-narrative/want-a-better-pitch-watch-this-328b95c2fd0b#.jkv1k4520)
  5. [Venture Hacks - Pitching Hacks](venture-hacks-pitching-hacks.pdf)
  6. [YCombinator - Guide To Demo Day Pitches](http://blog.ycombinator.com/guide-to-demo-day-pitches/)
  7. [Khan Academy - Pixar Storytelling Series](https://www.khanacademy.org/partner-content/pixar/storytelling)

## Design Thinking

  1. [Stanford d.school - Design Thinking Method Guide](https://raw.githubusercontent.com/joelmoxley/founder-playbook/master/I.%20Foundational/01%20-%20How%20To%20Tell%20Your%20Story/02%20-%20Content%20-%20Design%20Thinking/01%20-%20Stanford%20d.school%20-%20Design%20Thinking%20Method%20Guide.pdf)
  2. [Stanford d.school - Design Thinking Process Mode Guide](https://raw.githubusercontent.com/joelmoxley/founder-playbook/master/I.%20Foundational/01%20-%20How%20To%20Tell%20Your%20Story/02%20-%20Content%20-%20Design%20Thinking/02%20-%20Stanford%20d.school%20-%20Design%20Thinking%20Process%20Mode%20Guide.pdf)
  3. [Stanford d.school - Facilitator's Guide to Leading Re.d the G.G. Exp](https://raw.githubusercontent.com/joelmoxley/founder-playbook/master/I.%20Foundational/01%20-%20How%20To%20Tell%20Your%20Story/02%20-%20Content%20-%20Design%20Thinking/03%20-%20Stanford%20d.school%20-%20Facilitator's%20Guide%20to%20Leading%20Re.d%20the%20G.G.%20Exp.pdf)
  4. [Stanford d.school - Design + Entreprenueship (mayfield)](https://raw.githubusercontent.com/joelmoxley/founder-playbook/master/I.%20Foundational/01%20-%20How%20To%20Tell%20Your%20Story/02%20-%20Content%20-%20Design%20Thinking/04%20-%20Stanford%20d.school%20-%20Design%20%2B%20Entreprenueship%20(mayfield).ppt)