---
Title: VC Pitch
Play: 1
---

## Templates

  01. [Cyclotron Road Pitch Deck Template 2017](Cyclotron-Road-Pitch-Deck-Template-2017.pptx)

## EoE - Hard Tech Pitch Decks

  01. [Solar Junction Series A](Solar-Junction-Series-A.pdf)
  02. [Halotechnics Series Seed](Halotechnics-Series-Seed.pptx)

## EoE - Demo Day Pitch Decks

  01. [Polyspectra_Demo Day_Final](Polyspectra_Demo-Day_Final.pptx)
  02. [Opus 12_Demo Day_Final](Opus-12_Demo-Day_Final.pptx)
  03. [Feasible_Demo Day_Final](Feasible_Demo-Day_Final.pptx)
  04. [Cuberg_Demo Day_Final](Cuberg_Demo-Day_Final.pptx)
  05. [Mosaic Materials_Demo Day_Final](Mosaic-Materials_Demo-Day_Final.pptx)
  06. [Sepion_Demo Day_Final](Sepion_Demo-Day_Final.pptx)

## EoE - Software Pitch Deck Comparisons

  01. [AirBnB Series A Deck](AirBnB-Series-A-Deck.pdf)
  02. [LinkedIn Series B Deck](LinkedIn-Series-B-Deck.pdf)
  03. [Sequoia - Pitch Deck Structure](Sequoia-Pitch-Deck-Structure.pdf)

## One Pager

  01. [Cyclotron Road - Company One-Pagers](Cyclotron-Road-Company-One-Pagers.pptx)
  02. [Cyclotron Road - Company One-Pager Template](Cyclotron-Road-Company-One-Pager-Template.docx)




