---
title: Other Financing Including Charitable
play: 9
---

## Charitable Investment

  01. [ARPAE University - Charitable Investment For Energy Innovation](01-arpae-university-charitable-investment-for-energy-innovation.pdf)
  02. [ARPAE University - Charitable Investment For Energy Innovation](02-arpae-university-charitable-investment-for-energy-innovation.webloc)

## B Corporation

  01. [Certified B Corporation Site](01-certified-b-corporation-site.webloc)

