---
title: Govt Funding - SBIR
play: 4
---

## Opportunities and Approach

  01. [DOE SBIR Overview](01-doe-sbir-overview.webloc)
  02. [NSF SBIR Overview](02-nsf-sbir-overview.webloc)
  03. [NIH SBIR Overview](03-nih-sbir-overview.webloc)
  04. [DOD SBIR Overview](04-dod-sbir-overview.webloc)
  05. [National Innovation Summit To Network with SBIR PMs](05-national-innovation-summit-to-network-with-sbir-pms.webloc)

## Full Proposal

  01. [a - Halotechnics NSF SBIR Phase I Project Description](01-a-halotechnics-nsf-sbir-phase-i-project-description.doc)
  01. [b - Halotechnics NSF SBIR Phase I Reviewer Response](01-b-halotechnics-nsf-sbir-phase-i-reviewer-response.docx)
  01. [c - Halotechnics NSF SBIR Phase I Action Items Response](01-c-halotechnics-nsf-sbir-phase-i-action-items-response.docx)
  01. [d - Halotechnics NSF SBIR Phase II Project Description](01-d-halotechnics-nsf-sbir-phase-ii-project-description.docx)
  01. [e - Halotechnics NSF SBIR Phase II Commercialization Plan](01-e-halotechnics-nsf-sbir-phase-ii-commercialization-plan.docx)

