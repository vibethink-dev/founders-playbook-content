---
title: Company Building Theory
play: 3
---

## Company Building Theory

  01. [MIT Energy Ventures - Prioritizing & Filtering Ideas](01-mit-energy-ventures-prioritizing-filtering-ideas.pdf)
  02. [MIT Energy Ventures - COCA LTV IRR WACC](02-mit-energy-ventures-coca-ltv-irr-wacc.pdf)
  03. [Risk Matrix Presentation](03-risk-matrix-presentation.ppt)
  04. [C2M Go To Market Presentation](04-c2m-go-to-market-presentation.pptx)
  05. [C2M Developing Successful Products](05-c2m-developing-successful-products.pptx)
  06. [Stanford Entrep Course - Opportunity Assessment](06-stanford-entrep-course-opportunity-assessment.ppt)
  07. [KV Roach Crane Singh - Innovation Iteration Risk Mgmt](07-kv-roach-crane-singh-innovation-iteration-risk-mgmt.webloc)
  08. [Peter Thiel - Competition is for Losers](08-peter-thiel-competition-is-for-losers.webloc)
  09. [Thiel Zero To One - Derek Sivers Cliff Notes](09-thiel-zero-to-one-derek-sivers-cliff-notes.webloc)
  10. [Ries The Lean Startup Derek Sivers Cliff Notes](10-ries-the-lean-startup-derek-sivers-cliff-notes.webloc)
  11. [Peter Thiel - Competition Is for Losers](11-peter-thiel-competition-is-for-losers.pdf)

