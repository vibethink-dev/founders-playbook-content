---
title: Customer Discovery
play: 1
---

## ICorps Customer Discovery

  01. [ICorps - Best Practices Customer Discovery](01-icorps-best-practices-customer-discovery.pdf)
  02. [ARPAE University - ICorps Customer Discovery](02-arpae-university-icorps-customer-discovery.webloc)
  03. [Steve Blank - ARPAE Tech To Market](03-steve-blank-arpae-tech-to-market.pptx)
  04. [CD ICorps Customer Discovery](04-cd-icorps-customer-discovery.webloc)

