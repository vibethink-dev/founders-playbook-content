---
title: Sales Channel CAC + LTV
play: 7
---

## Sales Channel CAC + LTV Framework

  01. [Sales Channel CAC + LTV Framework Whitepaper](01-sales-channel-cac-ltv-framework-whitepaper.pdf)
  02. [Sales Channel CAC + LTV Framework Worksheet](02-sales-channel-cac-ltv-framework-worksheet.xlsx)

## Sales Channel CAC + LTV

  01. [a - Tren Griffin - Every Business Has a CAC](01-a-tren-griffin-every-business-has-a-cac.webloc)
  01. [b - Tren Griffin - Every Business Has a CAC](01-b-tren-griffin-every-business-has-a-cac.pdf)

## CRMs

  01. [Salesforce Relate IQ](01-salesforce-relate-iq.webloc)
  02. [Streak](02-streak.webloc)
  03. [DataFox](03-datafox.webloc)
  04. [Highrise](04-highrise.webloc)
  05. [TrackVia](05-trackvia.webloc)
  06. [Pipedrive](06-pipedrive.webloc)
  07. [Airtable](07-airtable.webloc)
  08. [Contactually](08-contactually.webloc)
  09. [Yesware](09-yesware.webloc)
  10. [Nimble](10-nimble.webloc)
  11. [Hubspot](11-hubspot.webloc)
  12. [Insightly](12-insightly.webloc)
  13. [Zoho](13-zoho.webloc)
  14. [Salesforce](14-salesforce.webloc)
  15. [Vero](15-vero.webloc)
  16. [Olark](16-olark.webloc)
  17. [Base](17-base.webloc)
  18. [InsideView](18-insideview.webloc)

