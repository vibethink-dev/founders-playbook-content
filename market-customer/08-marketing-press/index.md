---
title: Marketing + Press
play: 8
---

## Marketing

  01. [Stanford Entrep Course - Reality Marketing](01-stanford-entrep-course-reality-marketing.ppt)

## Press Execution

  01. [a - Rubicon Global - Wikipedia](01-a-rubicon-global-wikipedia.pdf)
  01. [b - Rubicon Global wiki references section](01-b-rubicon-global-wiki-references-section.webloc)
  02. [Nate Morris wiki references section](02-nate-morris-wiki-references-section.webloc)

