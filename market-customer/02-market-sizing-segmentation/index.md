---
title: Market Sizing + Segmentation
play: 2
---

## Market Segmentation

  01. [Khosla Ventures - Project Rifle](01-khosla-ventures-project-rifle.ppt)
  02. [Khosla Ventures - Project Rifle](02-khosla-ventures-project-rifle.pdf)
  03. [MIT Energy Ventures - Market Segmentation](03-mit-energy-ventures-market-segmentation.pdf)
  04. [Stanford Energy - Tanguy Chau - Market Segmentation](04-stanford-energy-tanguy-chau-market-segmentation.pptx)

## Market Requirements Documents

 *2007Wonica-Marketing requirements for high tech start-ups and business](-2007wonica-marketing-requirements-for-high-tech-start-ups-and-business.pdf)
  01. [Cyclotron Road - Market Requirements Document Template](01-cyclotron-road-market-requirements-document-template.docx)
 MRD template-1](mrd-template-1.dotx)

## Market Sizing S1

  2000. [Evergreen Solar](2000-evergreen-solar.webloc)
  2004. [Color Kinetics](2004-color-kinetics.webloc)
  2006. [Metabolix](2006-metabolix.webloc)
  2007. [Comverge](2007-comverge.webloc)
  2007. [EnerNOC](2007-enernoc.webloc)
  2007. [First Solar](2007-first-solar.webloc)
  2007. [Yingli](2007-yingli.webloc)
  2010. [A123](2010-a123.webloc)
  2010. [Amyris](2010-amyris.webloc)
  2010. [Codexis](2010-codexis.webloc)
  2010. [SemiLEDs](2010-semileds.webloc)
  2011. [Gevo](2011-gevo.webloc)
  2011. [KiOR](2011-kior.webloc)
  2011. [Myriant](2011-myriant.webloc)
  2011. [Solazyme](2011-solazyme.webloc)
  2011. [Tesla](2011-tesla.webloc)
  2012. [BrightSource](2012-brightsource.webloc)
  2012. [Ceres](2012-ceres.webloc)
  2012. [Enphase](2012-enphase.webloc)
  2012. [Genomatica](2012-genomatica.webloc)
  2012. [Luca Technologies](2012-luca-technologies.webloc)
  2012. [Mascoma](2012-mascoma.webloc)
  2013. [Silver Spring Networks](2013-silver-spring-networks.webloc)
  2014. [Aspen Aerogels](2014-aspen-aerogels.webloc)

